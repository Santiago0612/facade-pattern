import com.mitocode.facade.BusquedaHotel;
import com.mitocode.facade.BusquedaVuelos;

public class Facade {
    private BusquedaHotel apiUno;
     private BusquedaVuelos apiDos;

     public Facade(){
         apiUno=new BusquedaHotel();
         apiDos= new BusquedaVuelos();
     }
     public void buscar(String fechaIda, String fechaVuelta, String origuen, String destino){
         apiUno.buscarHoteles(fechaIda,fechaVuelta,origuen,destino);
        apiDos.Vuelos(fechaIda,fechaVuelta,origuen,destino);
     }
}
